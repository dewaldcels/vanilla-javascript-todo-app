
// importScripts('/cache-polyfill.js');

this.addEventListener('install', function(e) {
	e.waitUntil(
		caches.open('todoApp').then(function(cache) {
			return cache.addAll([
				'/',
				'/index.html',
				'/app.js',
				'/style.css',
                'https://fonts.googleapis.com/css?family=Dosis:400,700&display=swap',
                'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'
			]);
		})
	);
});

this.addEventListener('fetch', function (event) {
    // it can be empty if you just want to get rid of that error
});